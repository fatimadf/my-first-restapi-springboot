package com.example.myfirstrestapispringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirstRestapiSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirstRestapiSpringbootApplication.class, args);
	}

}
