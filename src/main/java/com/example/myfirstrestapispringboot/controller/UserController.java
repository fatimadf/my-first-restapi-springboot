package com.example.myfirstrestapispringboot.controller;

import com.example.myfirstrestapispringboot.domain.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/myapi/v1")
public class UserController {

    private List<User> person;


    @GetMapping("/users")
    public List<User> getAllUser(){
        return this.person;
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") long id) throws Exception{
        User user = null;

        for (User u:person) {
            if (u.getId()==id){
                user = u;
            }
        }

        if (user == null){
            throw new Exception("User not found");
        }
        return new ResponseEntity(user, HttpStatus.OK);
    }
    @PostMapping("/users")
    public String createUser(@RequestBody User user ){
        person.add(user);
        return "User added";
    }
    @DeleteMapping("/users/{id}")
    public String deleteUser(@PathVariable (value = "id") long id){
        for (int i = 0; i < person.size(); i++) {
            if (person.get(i).getId() == id){
                person.remove(i);
                return "User deleted";
            }

        }
        return "Invalid id";
    }
    @PutMapping("/users/{id}")
    public String updateUser(@RequestBody User user,  @PathVariable (value = "id") long id){

        for (User u : person) {
             if (u.getId()==id){

                 u.setFirstName(user.getFirstName());
                 u.setLastName(user.getLastName());
                 u.setEmail(user.getEmail());

                 return "User update";
             }
        }

        return "Invalid id";

    }

    @PostConstruct
    private void startstorage (){

        this.person = new ArrayList<>();

        person.add(new User(1, "Fatima", "Del Fabro", "fati@gmail.com" ));
    }

}

